package com.example.lorencoo.fdsapp.activity.teams;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.activity.players.PlayersActivity;
import com.example.lorencoo.fdsapp.data.data.team.Teams;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TeamsAdapter extends RecyclerView.Adapter<TeamsAdapter.ViewHolder> {


    public static final String PLAYERS_LINK = "PLAYERS_LINK";
    private List<Teams> teamsList = new ArrayList<>();
    private int selectedItemPosition = -1;

    public void updateTeams(List<Teams> teams) {
        teamsList.clear();
        teamsList.addAll(teams);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_teams, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setupTeams(teamsList.get(position));

    }

    @Override
    public int getItemCount() {
        return teamsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_teams_team_name)
        TextView teamName;
        @BindView(R.id.item_teams_logo)
        ImageView logo;
        @BindView(R.id.item_teams_players_button)
        Button playersButton;

        private String playersLink;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setupTeams(Teams teamsList) {

            Glide.with(itemView.getContext())
                    .load(teamsList.crestUrl)
                    .apply(new RequestOptions().placeholder(R.drawable.ic_launcher_background))
                    .into(logo);

            teamName.setText(teamsList.name);

            playersLink = teamsList.link.teamPlayers.href;

            selectedTeams();
        }

        @OnClick(R.id.item_teams_players_button)
        public void openPlayersActivity() {

            String playersLinkReplacePrefix = playersLink.replace("http://api.football-data.org/v1/teams/", "");
            String playersLinkReplacePlayers = playersLinkReplacePrefix.replace("/players", "");

            Intent intentFixtures = new Intent(itemView.getContext(), PlayersActivity.class);
            intentFixtures.putExtra(PLAYERS_LINK, playersLinkReplacePlayers);
            itemView.getContext().startActivity(intentFixtures);
        }

        private void selectedTeams() {
            if (selectedItemPosition == getAdapterPosition()) {
                playersButton.setVisibility(View.VISIBLE);
            } else {
                playersButton.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(view -> {
                int currentPosition = getAdapterPosition();

                if (currentPosition == selectedItemPosition) {
                    selectedItemPosition = -1;
                    notifyItemChanged(currentPosition);
                } else {
                    final int previouslySelectedItem = selectedItemPosition;
                    selectedItemPosition = -1;
                    notifyItemChanged(previouslySelectedItem);
                    selectedItemPosition = currentPosition;
                    notifyItemChanged(currentPosition);
                }
            });
        }

    }
}
