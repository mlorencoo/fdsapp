package com.example.lorencoo.fdsapp.di;


import com.example.lorencoo.fdsapp.ApplicationScope;
import com.example.lorencoo.fdsapp.activity.competition.CompetitionComponent;
import com.example.lorencoo.fdsapp.activity.competition.CompetitionModule;
import com.example.lorencoo.fdsapp.activity.fixtures.FixturesComponent;
import com.example.lorencoo.fdsapp.activity.fixtures.FixturesModule;
import com.example.lorencoo.fdsapp.activity.players.PlayersComponent;
import com.example.lorencoo.fdsapp.activity.players.PlayersModule;
import com.example.lorencoo.fdsapp.activity.table.TableComponent;
import com.example.lorencoo.fdsapp.activity.table.TableModule;
import com.example.lorencoo.fdsapp.activity.teams.TeamsComponent;
import com.example.lorencoo.fdsapp.activity.teams.TeamsModule;

import dagger.Component;

@ApplicationScope
@Component(modules = {AppModule.class, DataModule.class})
public interface AppComponent {
    CompetitionComponent plus(CompetitionModule competitionModule);
    FixturesComponent plus(FixturesModule fixturesModule);
    TableComponent plus(TableModule tableModule);
    TeamsComponent plus(TeamsModule teamsModule);
    PlayersComponent plus(PlayersModule playersModule);
}
