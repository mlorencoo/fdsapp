package com.example.lorencoo.fdsapp.activity.teams;

import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.lorencoo.fdsapp.FDSApplication;
import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.activity.competition.CompetitionAdapter;
import com.example.lorencoo.fdsapp.data.data.team.Teams;
import com.example.lorencoo.fdsapp.menus.BaseMenuActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TeamsActivity extends BaseMenuActivity implements TeamsContract.View {
    @BindView(R.id.swipe_refresh_teams)
    SwipeRefreshLayout swipeRefreshTeams;

    @BindView(R.id.recycler_teams)
    RecyclerView teamsRecycler;

    @BindView(R.id.progres_bar_teams)
    ProgressBar teamsProgress;

    @BindView(R.id.teams_error_group)
    Group teamsErrorGroup;

    private TeamsAdapter teamsAdapter;

    @Inject
    TeamsContract.Presenter presenter;


    @OnClick(R.id.teams_button)
    public void onTryAgainClick() {
        presenter.onTryAgainClick();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teams);
        ButterKnife.bind(this);

        ((FDSApplication)getApplication()).getAppComponent()
                .plus(new TeamsModule(this))
                .inject(this);

        presenter.setCompetitionId(getIntent().getStringExtra(CompetitionAdapter.COMPETITION_ID));

        teamsAdapter = new TeamsAdapter();

        teamsRecycler.setLayoutManager(new LinearLayoutManager(this));
        teamsRecycler.setAdapter(teamsAdapter);

        swipeRefreshTeams.setOnRefreshListener(() -> presenter.doYourUpdate());
    }

    @Override
    public void showError() {
        swipeRefreshTeams.setRefreshing(false);
        teamsRecycler.setVisibility(View.INVISIBLE);
        teamsProgress.setVisibility(View.INVISIBLE);
        teamsErrorGroup.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData(List<Teams> teamsList) {
        swipeRefreshTeams.setRefreshing(false);
        teamsProgress.setVisibility(View.INVISIBLE);
        teamsErrorGroup.setVisibility(View.INVISIBLE);
        teamsRecycler.setVisibility(View.VISIBLE);
        teamsAdapter.updateTeams(teamsList);
    }

    @Override
    public void showProgress() {
        teamsRecycler.setVisibility(View.INVISIBLE);
        teamsErrorGroup.setVisibility(View.INVISIBLE);
        teamsProgress.setVisibility(View.VISIBLE);
    }
}
