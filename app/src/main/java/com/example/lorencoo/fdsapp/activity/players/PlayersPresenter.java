package com.example.lorencoo.fdsapp.activity.players;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.example.lorencoo.fdsapp.api.FootballApi;
import com.example.lorencoo.fdsapp.dao.MyDao;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class PlayersPresenter implements PlayersContract.Presenter, LifecycleObserver {

    private PlayersContract.View view;
    private FootballApi footballApi;
    private MyDao myDao;

    private String teamsId;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public PlayersPresenter(PlayersContract.View view, FootballApi footballApi, MyDao myDao) {
        this.view = view;
        this.footballApi = footballApi;
        ((LifecycleOwner) this.view).getLifecycle().addObserver(this);
        this.myDao = myDao;
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onStart() {
        getPlayers();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onDestroy() {
        compositeDisposable.clear();
    }


    @Override
    public void onTryAgainClick() {
        getPlayers();
    }

    @Override
    public void doYourUpdate() {
        getPlayers();
    }

    @Override
    public void setTeamsId(String teamsId) {
        this.teamsId = teamsId;
    }

    private void getPlayers() {
        view.showProgress();
        if (myDao.getPlayersResponseDao(teamsId) == null) {
            compositeDisposable.add(footballApi.getPlayersResponseApi(teamsId)
                    .subscribeOn(Schedulers.io())
                    .flatMapObservable(playersResponse -> {
                        playersResponse.setId(teamsId);
                        myDao.insertPlayersDao(playersResponse);
                        return Observable.just(playersResponse);
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            playersResponse ->
                                    view.showData(playersResponse.playersList),
                            throwable ->
                                    view.showError()
                    )
            );
        } else {
            view.showData(myDao.getPlayersResponseDao(teamsId).playersList);
        }

    }
}