package com.example.lorencoo.fdsapp.activity.teams;


import com.example.lorencoo.fdsapp.data.data.team.Teams;

import java.util.List;

public interface TeamsContract {
    interface View {
        void showError();

        void showProgress();

        void showData(List<Teams> teamsList);
    }

    interface Presenter {
        void onTryAgainClick();

        void doYourUpdate();

        void setCompetitionId(String competitionId);
    }
}
