package com.example.lorencoo.fdsapp.converters;

import android.arch.persistence.room.TypeConverter;

import com.example.lorencoo.fdsapp.data.data.players.Players;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class PlayersGsonConventer {
    @TypeConverter
    public static List<Players> stringToPlayers(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Players>>() {
        }.getType();
        List<Players> players = gson.fromJson(json, type);
        return players;
    }

    @TypeConverter
    public static String playersToSting(List<Players> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Players>>() {
        }.getType();
        String json = gson.toJson(list, type);
        return json;
    }
}
