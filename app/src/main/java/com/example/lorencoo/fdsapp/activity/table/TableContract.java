package com.example.lorencoo.fdsapp.activity.table;


import com.example.lorencoo.fdsapp.data.data.table.Standing;

import java.util.List;

public interface TableContract {
    interface View {
        void showError();

        void showProgress();

        void showData(List<Standing> standingList);
    }

    interface Presenter {
        void onTryAgainClick();

        void doYourUpdate();

        void setCompetitionId(String competitionId);
    }
}
