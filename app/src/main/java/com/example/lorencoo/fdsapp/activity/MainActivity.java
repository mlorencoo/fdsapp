package com.example.lorencoo.fdsapp.activity;

import android.content.Intent;
import android.os.Bundle;

import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.activity.competition.CompetitionActivity;
import com.example.lorencoo.fdsapp.menus.BaseMenuActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.open_competition_activity)
    public void openCompetionActivity() {
        startActivity(new Intent(this, CompetitionActivity.class));
    }

    @OnClick(R.id.open_flashscore_activity)
    public void openFlashScoreActivity() {
        startActivity(new Intent(this, FlashScoreActivity.class));
    }


}
