package com.example.lorencoo.fdsapp.activity.fixtures;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.data.data.fixtures.Fixtures;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FixturesAdapter extends RecyclerView.Adapter<FixturesAdapter.ViewHolder> {

    private List<Fixtures> fixturesList = new ArrayList<>();

    public void updateFixtures(List<Fixtures> fixtures) {
        fixturesList.clear();
        fixturesList.addAll(fixtures);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_fixtures, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setupFixture(fixturesList.get(position));

    }

    @Override
    public int getItemCount() {
        return fixturesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_teams_card_view)
        CardView cardView;
        @BindView(R.id.item_teams_team_name)
        TextView homeTeamName;
        @BindView(R.id.item_fixtures_home_team_goals)
        TextView homeTeamGoals;
        @BindView(R.id.item_fixtures_away_team_goals)
        TextView awayTeamGoals;
        @BindView(R.id.item_fixtures_away_team_name)
        TextView awayTeamName;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setupFixture(Fixtures fixturesList) {
            homeTeamName.setText(fixturesList.homeTeamName);
            homeTeamGoals.setText(fixturesList.result.goalsHomeTeam);
            awayTeamGoals.setText(fixturesList.result.goalsAwayTeam);
            awayTeamName.setText(fixturesList.awayTeamName);
        }

    }
}
