package com.example.lorencoo.fdsapp.activity.teams;

import com.example.lorencoo.fdsapp.api.FootballApi;
import com.example.lorencoo.fdsapp.dao.MyDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class TeamsModule {
    private TeamsContract.View view;

    public TeamsModule(TeamsContract.View view) {
        this.view = view;

    }
    @Provides
    @Singleton
    TeamsContract.Presenter provideTeamsPresenter(FootballApi footballApi, MyDao myDao){
        return new TeamsPresenter(view,footballApi,myDao);
    }
}
