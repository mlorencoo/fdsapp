package com.example.lorencoo.fdsapp.activity.fixtures;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {FixturesModule.class})
public interface FixturesComponent {
    void inject(FixturesActivity fixturesActivity);
}
