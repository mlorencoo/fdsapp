package com.example.lorencoo.fdsapp.activity.competition;


import com.example.lorencoo.fdsapp.data.data.Competition;

import java.util.List;

public interface CompetitionContract {
    interface View {
        void showError();

        void showData(List<Competition> competitions);

        void showProgress();
    }

    interface Presenter {
        void onTryAgainClick();

        void doYourUpdate();
    }
}
