package com.example.lorencoo.fdsapp.converters;

import android.arch.persistence.room.TypeConverter;

import com.example.lorencoo.fdsapp.data.data.table.Standing;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class TableGsonConventer {
    @TypeConverter
    public static List<Standing> stringToStanding(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Standing>>() {}.getType();
        List<Standing> standings = gson.fromJson(json, type);
        return standings;
    }

    @TypeConverter
    public static String standingsToSting(List<Standing> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Standing>>() {}.getType();
        String json = gson.toJson(list, type);
        return json;
    }
}
