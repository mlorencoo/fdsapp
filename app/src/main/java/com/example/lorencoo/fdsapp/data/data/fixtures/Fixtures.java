package com.example.lorencoo.fdsapp.data.data.fixtures;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;


@Entity(primaryKeys = {"homeTeamName", "awayTeamName","matchday"})
public class Fixtures {
    public int matchday;

    @NonNull
    public String homeTeamName;

    @NonNull
    public String awayTeamName;


    @Embedded
    public Result result=new Result();

}
