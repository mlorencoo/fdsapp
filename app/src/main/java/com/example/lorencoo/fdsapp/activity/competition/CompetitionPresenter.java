package com.example.lorencoo.fdsapp.activity.competition;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.example.lorencoo.fdsapp.api.FootballApi;
import com.example.lorencoo.fdsapp.dao.MyDao;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CompetitionPresenter implements CompetitionContract.Presenter, LifecycleObserver {
    private CompetitionContract.View view;
    private FootballApi footballApi;
    private MyDao myDao;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public CompetitionPresenter(CompetitionContract.View view, FootballApi footballApi,MyDao myDao ) {
        this.view = view;
        this.footballApi = footballApi;
        ((LifecycleOwner) this.view).getLifecycle().addObserver(this);
        this.myDao=myDao;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onStart() {
        getCompetitions();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onDestroy() {
        compositeDisposable.clear();
    }


    @Override
    public void onTryAgainClick() {
        getCompetitions();
    }

    @Override
    public void doYourUpdate() {
        getCompetitions();
    }

    private void getCompetitions() {
        view.showProgress();
        if (myDao.getAllCompetitionDao().isEmpty()) {
            compositeDisposable.add(footballApi.getCompetitionsApi()
                    .subscribeOn(Schedulers.io())
                    .flatMapObservable(competitions -> {
                        myDao.insertCompetitionDao(competitions);
                        return Observable.just(competitions);
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            competitions -> view.showData(competitions),
                            throwable -> view.showError()
                    )
            );
        }
        else {
            view.showData(myDao.getAllCompetitionDao());
        }
    }
}
