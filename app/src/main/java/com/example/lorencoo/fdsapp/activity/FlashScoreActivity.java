package com.example.lorencoo.fdsapp.activity;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.menus.BaseMenuActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FlashScoreActivity extends BaseMenuActivity {

    @BindView(R.id.webView)
    WebView wb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_score);
        ButterKnife.bind(this);

        wb.getSettings().setJavaScriptEnabled(true);
        wb.getSettings().setLoadWithOverviewMode(true);
        wb.getSettings().setUseWideViewPort(true);
        wb.getSettings().setBuiltInZoomControls(true);
        wb.setWebViewClient(new WebViewClient());
        wb.loadUrl("https://www.flashscore.com/");
    }
}
