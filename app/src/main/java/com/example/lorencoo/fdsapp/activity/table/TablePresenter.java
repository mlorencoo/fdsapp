package com.example.lorencoo.fdsapp.activity.table;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.example.lorencoo.fdsapp.api.FootballApi;
import com.example.lorencoo.fdsapp.dao.MyDao;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class TablePresenter implements TableContract.Presenter, LifecycleObserver {
    private TableContract.View view;
    private FootballApi footballApi;
    private MyDao myDao;
    private String  competitionId;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public TablePresenter(TableContract.View view, FootballApi footballApi, MyDao myDao ) {
        this.view = view;
        this.footballApi = footballApi;
        ((LifecycleOwner) this.view).getLifecycle().addObserver(this);
        this.myDao=myDao;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onStart() {
        getTable();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onDestroy() {
        compositeDisposable.clear();
    }


    @Override
    public void onTryAgainClick() {
        getTable();
    }

    @Override
    public void doYourUpdate() {
        getTable();
    }

    @Override
    public void setCompetitionId(String competitionId) {
        this.competitionId=competitionId;
    }


    private void getTable() {
        view.showProgress();
        if (myDao.getTableResponseDao(competitionId)==null) {
            compositeDisposable.add(footballApi.getTableResponseApi(competitionId)
                    .subscribeOn(Schedulers.io())
                    .flatMapObservable(tableResponse -> {
                        tableResponse.setId(competitionId);
                        myDao.insertTableDao(tableResponse);
                        return Observable.just(tableResponse);
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            tableResponse -> view.showData(tableResponse.standingList),
                            throwable -> view.showError()
                    )
            );
        } else {
            view.showData(myDao.getTableResponseDao(competitionId).standingList);
        }
    }
}
