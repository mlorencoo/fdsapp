package com.example.lorencoo.fdsapp.activity.competition;

import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.lorencoo.fdsapp.FDSApplication;
import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.data.data.Competition;
import com.example.lorencoo.fdsapp.menus.BaseMenuActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CompetitionActivity extends BaseMenuActivity implements CompetitionContract.View {

    @BindView(R.id.swipe_refresh_competition)
    SwipeRefreshLayout swipeRefreshCompetition;

    @BindView(R.id.recycler_competition)
    RecyclerView competitionRecycler;

    @BindView(R.id.progres_bar_competition)
    ProgressBar competitionProgress;

    @BindView(R.id.competition_error_group)
    Group competitionErrorGroup;

    private CompetitionAdapter competitionAdapter;

    @Inject
    CompetitionContract.Presenter presenter;

    @OnClick(R.id.competition_button)
    public void onTryAgainClick() {
        presenter.onTryAgainClick();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competition);
        ButterKnife.bind(this);


        ((FDSApplication)getApplication()).getAppComponent()
                .plus(new CompetitionModule(this))
                .inject(this);

        competitionAdapter = new CompetitionAdapter();

        competitionRecycler.setLayoutManager(new LinearLayoutManager(this));
        competitionRecycler.setAdapter(competitionAdapter);

        swipeRefreshCompetition.setOnRefreshListener(() -> presenter.doYourUpdate());
    }

    @Override
    public void showError() {
        swipeRefreshCompetition.setRefreshing(false);
        competitionProgress.setVisibility(View.INVISIBLE);
        competitionRecycler.setVisibility(View.INVISIBLE);
        competitionErrorGroup.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData(List<Competition> competitions) {
        swipeRefreshCompetition.setRefreshing(false);
        competitionProgress.setVisibility(View.INVISIBLE);
        competitionErrorGroup.setVisibility(View.INVISIBLE);
        competitionRecycler.setVisibility(View.VISIBLE);
        competitionAdapter.updateCompetitions(competitions);
    }

    @Override
    public void showProgress() {
        competitionRecycler.setVisibility(View.INVISIBLE);
        competitionErrorGroup.setVisibility(View.INVISIBLE);
        competitionProgress.setVisibility(View.VISIBLE);
    }
}
