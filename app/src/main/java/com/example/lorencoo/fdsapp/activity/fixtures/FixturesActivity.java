package com.example.lorencoo.fdsapp.activity.fixtures;

import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.lorencoo.fdsapp.FDSApplication;
import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.activity.competition.CompetitionAdapter;
import com.example.lorencoo.fdsapp.data.data.fixtures.Fixtures;
import com.example.lorencoo.fdsapp.menus.BaseMenuActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FixturesActivity extends BaseMenuActivity implements FixturesContract.View {
    @BindView(R.id.swipe_refresh_fixtures)
    SwipeRefreshLayout swipeRefreshFixtures;

    @BindView(R.id.recycler_fixtures)
    RecyclerView fixturesRecycler;

    @BindView(R.id.progres_bar_fixtures)
    ProgressBar fixturesProgress;

    @BindView(R.id.fixtures_error_group)
    Group fixturesErrorGroup;

    @BindView(R.id.search_fixtures)
    EditText searchFixtures;
    @BindView(R.id.animation_search)
    ImageView animationSearch;


    private FixturesAdapter fixturesAdapter;

    @Inject
    FixturesContract.Presenter presenter;


    @OnClick(R.id.fixtures_button)
    public void onTryAgainClick() {
        presenter.onTryAgainClick();
    }

    @OnClick(R.id.search_fixtures)
    public void searchFixturesByTeam() {

        searchFixtures.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!searchFixtures.getText().toString().isEmpty()) {
                    String teamName = searchFixtures.getText().toString();
                    presenter.searchByTeam(teamName);
                } else presenter.doYourUpdate();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fixtures);
        ButterKnife.bind(this);

        ((FDSApplication) getApplication()).getAppComponent()
                .plus(new FixturesModule(this))
                .inject(this);

        presenter.setCompetitionId(getIntent().getStringExtra(CompetitionAdapter.COMPETITION_ID));

        fixturesAdapter = new FixturesAdapter();

        fixturesRecycler.setLayoutManager(new LinearLayoutManager(this));
        fixturesRecycler.setAdapter(fixturesAdapter);

        swipeRefreshFixtures.setOnRefreshListener(() -> presenter.doYourUpdate());

        animate(this.searchFixtures);

    }

    public void animate(View view) {
        AnimatedVectorDrawable searchToBar = (AnimatedVectorDrawable) getResources().getDrawable(R.drawable.anim_search_to_bar);
        Interpolator interp = AnimationUtils.loadInterpolator(this, android.R.interpolator.linear_out_slow_in);
        int duration = 1000;
        float offset = -71f * (int) getResources().getDisplayMetrics().scaledDensity;
        animationSearch.setTranslationX(offset);
        animationSearch.setImageDrawable(searchToBar);
        searchToBar.start();
        animationSearch.animate().translationX(0f).setDuration(duration).setInterpolator(interp);
        searchFixtures.animate().alpha(1f).setStartDelay(duration - 100).setDuration(100).setInterpolator(interp);
    }


    @Override
    public void showError() {
        swipeRefreshFixtures.setRefreshing(false);
        fixturesRecycler.setVisibility(View.INVISIBLE);
        fixturesProgress.setVisibility(View.INVISIBLE);
        fixturesErrorGroup.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData(List<Fixtures> fixturesList) {
        swipeRefreshFixtures.setRefreshing(false);
        fixturesProgress.setVisibility(View.INVISIBLE);
        fixturesErrorGroup.setVisibility(View.INVISIBLE);
        fixturesRecycler.setVisibility(View.VISIBLE);
        fixturesAdapter.updateFixtures(fixturesList);
    }

    @Override
    public void showProgress() {
        fixturesRecycler.setVisibility(View.INVISIBLE);
        fixturesErrorGroup.setVisibility(View.INVISIBLE);
        fixturesProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void showDataByTeam(List<Fixtures> fixtureByTeam) {
        swipeRefreshFixtures.setRefreshing(false);
        fixturesProgress.setVisibility(View.INVISIBLE);
        fixturesErrorGroup.setVisibility(View.INVISIBLE);
        fixturesRecycler.setVisibility(View.VISIBLE);
        fixturesAdapter.updateFixtures(fixtureByTeam);
    }
}
