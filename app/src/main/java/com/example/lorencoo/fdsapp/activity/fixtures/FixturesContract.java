package com.example.lorencoo.fdsapp.activity.fixtures;


import com.example.lorencoo.fdsapp.data.data.fixtures.Fixtures;

import java.util.List;

public interface FixturesContract {
    interface View {
        void showError();

        void showData(List<Fixtures> fixturesList);

        void showProgress();

        void showDataByTeam(List<Fixtures> fixtureByTeam);
    }

    interface Presenter {
        void onTryAgainClick();

        void doYourUpdate();

        void setCompetitionId(String competitionId);

        void searchByTeam(String teamName);
    }
}
