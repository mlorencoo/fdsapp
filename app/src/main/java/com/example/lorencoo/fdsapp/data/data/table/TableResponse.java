package com.example.lorencoo.fdsapp.data.data.table;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.example.lorencoo.fdsapp.converters.TableGsonConventer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
@Entity
@TypeConverters(TableGsonConventer.class)
public class TableResponse {

    @PrimaryKey
    @NonNull
    public String matchday;

    public String leagueCaption;

    @SerializedName("standing")
    public List< Standing> standingList = new ArrayList<>();

    @Expose(deserialize = false, serialize = false)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
