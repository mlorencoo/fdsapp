package com.example.lorencoo.fdsapp.activity.table;

import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.lorencoo.fdsapp.FDSApplication;
import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.activity.competition.CompetitionAdapter;
import com.example.lorencoo.fdsapp.data.data.table.Standing;
import com.example.lorencoo.fdsapp.menus.BaseMenuActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TableActivity extends BaseMenuActivity implements TableContract.View {
    @BindView(R.id.swipe_refresh_table)
    SwipeRefreshLayout swipeRefreshTable;

    @BindView(R.id.recycler_table)
    RecyclerView tableRecycler;

    @BindView(R.id.progres_bar_table)
    ProgressBar tableProgress;

    @BindView(R.id.table_error_group)
    Group tableErrorGroup;

    private TableAdapter tableAdapter;

    @Inject
    TableContract.Presenter presenter;


    @OnClick(R.id.table_button)
    public void onTryAgainClick() {
        presenter.onTryAgainClick();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        ButterKnife.bind(this);

        ((FDSApplication)getApplication()).getAppComponent()
                .plus(new TableModule(this))
                .inject(this);

        presenter.setCompetitionId(getIntent().getStringExtra(CompetitionAdapter.COMPETITION_ID));


        tableAdapter = new TableAdapter();

        tableRecycler.setLayoutManager(new LinearLayoutManager(this));
        tableRecycler.setAdapter(tableAdapter);

        swipeRefreshTable.setOnRefreshListener(() -> presenter.doYourUpdate());
    }

    @Override
    public void showError() {
        swipeRefreshTable.setRefreshing(false);
        tableRecycler.setVisibility(View.INVISIBLE);
        tableProgress.setVisibility(View.INVISIBLE);
        tableErrorGroup.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData(List<Standing> standingList) {
        swipeRefreshTable.setRefreshing(false);
        tableProgress.setVisibility(View.INVISIBLE);
        tableErrorGroup.setVisibility(View.INVISIBLE);
        tableRecycler.setVisibility(View.VISIBLE);
        tableAdapter.updateTable(standingList);
    }

    @Override
    public void showProgress() {
        tableRecycler.setVisibility(View.INVISIBLE);
        tableErrorGroup.setVisibility(View.INVISIBLE);
        tableProgress.setVisibility(View.VISIBLE);
    }
}
