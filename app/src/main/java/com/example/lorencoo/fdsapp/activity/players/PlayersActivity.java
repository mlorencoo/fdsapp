package com.example.lorencoo.fdsapp.activity.players;

import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.lorencoo.fdsapp.FDSApplication;
import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.activity.teams.TeamsAdapter;
import com.example.lorencoo.fdsapp.data.data.players.Players;
import com.example.lorencoo.fdsapp.menus.BaseMenuActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlayersActivity extends BaseMenuActivity implements PlayersContract.View {

    @BindView(R.id.swipe_refresh_players)
    SwipeRefreshLayout swipeRefreshPlayers;

    @BindView(R.id.recycler_players)
    RecyclerView playersRecycler;

    @BindView(R.id.progres_bar_players)
    ProgressBar playersProgress;

    @BindView(R.id.players_error_group)
    Group playersErrorGroup;

    private PlayersAdapter playersAdapter;

    @Inject
    PlayersContract.Presenter presenter;

    @OnClick(R.id.players_button)
    public void onTryAgainClick() {
        presenter.onTryAgainClick();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players);
        ButterKnife.bind(this);

        ((FDSApplication) getApplication()).getAppComponent()
                .plus(new PlayersModule(this))
                .inject(this);

        presenter.setTeamsId(getIntent().getStringExtra(TeamsAdapter.PLAYERS_LINK));

        playersAdapter = new PlayersAdapter();

        playersRecycler.setLayoutManager(new LinearLayoutManager(this));
        playersRecycler.setAdapter(playersAdapter);

        swipeRefreshPlayers.setOnRefreshListener(() ->
                presenter.doYourUpdate());
    }

    @Override
    public void showError() {
        swipeRefreshPlayers.setRefreshing(false);
        playersProgress.setVisibility(View.INVISIBLE);
        playersRecycler.setVisibility(View.INVISIBLE);
        playersErrorGroup.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        playersRecycler.setVisibility(View.INVISIBLE);
        playersErrorGroup.setVisibility(View.INVISIBLE);
        playersProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData(List<Players> players) {
        swipeRefreshPlayers.setRefreshing(false);
        playersProgress.setVisibility(View.INVISIBLE);
        playersErrorGroup.setVisibility(View.INVISIBLE);
        playersRecycler.setVisibility(View.VISIBLE);
        playersAdapter.updatePlayers(players);
    }
}
