package com.example.lorencoo.fdsapp.di;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.lorencoo.fdsapp.ApplicationScope;
import com.example.lorencoo.fdsapp.api.FootballApi;
import com.example.lorencoo.fdsapp.dao.MyDao;
import com.example.lorencoo.fdsapp.database.AppDatabase;
import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DataModule {


    @Provides
    @ApplicationScope
    AppDatabase provideAppDataBase(Context context) {
        return Room.databaseBuilder(context,
                AppDatabase.class, "database-name")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @ApplicationScope
    MyDao providesMyDao(AppDatabase appDatabase) {
        return appDatabase.myDao();
    }

    @Provides
    @ApplicationScope
    Retrofit providesRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(FootballApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }
    @Provides
    @ApplicationScope
    FootballApi providesFootballApi(Retrofit retrofit){
        return retrofit.create(FootballApi.class);
    }
}
