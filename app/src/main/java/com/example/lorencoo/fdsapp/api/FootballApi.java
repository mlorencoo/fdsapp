package com.example.lorencoo.fdsapp.api;

import com.example.lorencoo.fdsapp.data.data.Competition;
import com.example.lorencoo.fdsapp.data.data.fixtures.FixtureResponse;
import com.example.lorencoo.fdsapp.data.data.players.PlayersResponse;
import com.example.lorencoo.fdsapp.data.data.table.TableResponse;
import com.example.lorencoo.fdsapp.data.data.team.TeamResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface FootballApi {

    String BASE_URL = "http://api.football-data.org/v1/";


    @Headers("X-Auth-Token: f3b4f6f151054f69842c2c502b6a560d")
    @GET("competitions")
    Single<List<Competition>> getCompetitionsApi();

    @Headers("X-Auth-Token: f3b4f6f151054f69842c2c502b6a560d")
    @GET("competitions/{id}/fixtures")
    Single<FixtureResponse> getFixturesResponseApi(@Path("id") String id);

    @Headers("X-Auth-Token: f3b4f6f151054f69842c2c502b6a560d")
    @GET("competitions/{id}/teams")
    Single<TeamResponse> getTeamsResponseApi(@Path("id") String id);

    @Headers("X-Auth-Token: f3b4f6f151054f69842c2c502b6a560d")
    @GET("competitions/{id}/leagueTable")
    Single<TableResponse> getTableResponseApi(@Path("id") String id);

    @Headers("X-Auth-Token: f3b4f6f151054f69842c2c502b6a560d")
    @GET("teams/{id}/players")
    Single<PlayersResponse> getPlayersResponseApi(@Path("id") String id);

}
