package com.example.lorencoo.fdsapp.activity.players;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.data.data.players.Players;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlayersAdapter extends RecyclerView.Adapter<PlayersAdapter.ViewHolder> {

    private List<Players> playersList = new ArrayList<>();

    private int selectedItemPosition = -1;

    public void updatePlayers(List<Players> players) {
        playersList.clear();
        playersList.addAll(players);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_players, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.setupPlayers(playersList.get(position));
    }

    @Override
    public int getItemCount() {
        return playersList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_players_name)
        TextView namePlayer;
        @BindView(R.id.linear_layout_players_gone)
        LinearLayout linearLayoutPlayer;
        @BindView(R.id.item_players_position)
        TextView positionPlayer;
        @BindView(R.id.item_players_number)
        TextView numberPlayer;
        @BindView(R.id.item_players_date_birth)
        TextView dateBirthPLayer;
        @BindView(R.id.item_players_nationality)
        TextView nationalityPlayer;
        @BindView(R.id.item_players_contract_until)
        TextView contractPlayer;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void setupPlayers(Players playersList) {

            namePlayer.setText(playersList.getName());
            positionPlayer.setText(playersList.getPosition());
            numberPlayer.setText(String.valueOf(playersList.getJerseyNumber()));
            dateBirthPLayer.setText(playersList.getDateOfBirth());
            nationalityPlayer.setText(playersList.getNationality());
            contractPlayer.setText(playersList.getContractUntil());

            selectedPlayer();
        }

        private void selectedPlayer() {
            if (selectedItemPosition == getAdapterPosition()) {
                linearLayoutPlayer.setVisibility(View.VISIBLE);
            } else {
                linearLayoutPlayer.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(view -> {
                int currentPosition = getAdapterPosition();

                if (currentPosition == selectedItemPosition) {
                    selectedItemPosition = -1;
                    notifyItemChanged(currentPosition);
                } else {
                    final int previouslySelectedItem = selectedItemPosition;
                    selectedItemPosition = -1;
                    notifyItemChanged(previouslySelectedItem);
                    selectedItemPosition = currentPosition;
                    notifyItemChanged(currentPosition);
                }
            });
        }
    }
}
