package com.example.lorencoo.fdsapp.data.data.team;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity
public class Teams {

    @PrimaryKey
    public String name;

    public String crestUrl;

    @Embedded
    @SerializedName("_links")
    public Link link = new Link();
}
