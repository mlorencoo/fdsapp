package com.example.lorencoo.fdsapp.activity.players;

import com.example.lorencoo.fdsapp.api.FootballApi;
import com.example.lorencoo.fdsapp.dao.MyDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PlayersModule {
    private PlayersContract.View view;

    public PlayersModule(PlayersContract.View view) {
        this.view = view;

    }
    @Provides
    @Singleton
    PlayersContract.Presenter providePlayersPresenter(FootballApi footballApi, MyDao myDao){
        return new PlayersPresenter(view,footballApi,myDao);
    }
}
