package com.example.lorencoo.fdsapp.converters;

import android.arch.persistence.room.TypeConverter;

import com.example.lorencoo.fdsapp.data.data.team.Teams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class TeamsGsonConventer {
    @TypeConverter
    public static List<Teams> stringToTeams(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Teams>>() {}.getType();
        List<Teams> teams = gson.fromJson(json, type);
        return teams;
    }

    @TypeConverter
    public static String teamsToSting(List<Teams> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Teams>>() {}.getType();
        String json = gson.toJson(list, type);
        return json;
    }
}
