package com.example.lorencoo.fdsapp.activity.table;

import com.example.lorencoo.fdsapp.api.FootballApi;
import com.example.lorencoo.fdsapp.dao.MyDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class TableModule {
    private TableContract.View view;

    public TableModule(TableContract.View view) {
        this.view = view;

    }
    @Provides
    @Singleton
    TableContract.Presenter provideTablePresenter(FootballApi footballApi, MyDao myDao){
        return new TablePresenter(view,footballApi,myDao);
    }
}
