package com.example.lorencoo.fdsapp.activity.competition;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {CompetitionModule.class})
public interface CompetitionComponent {
    void inject(CompetitionActivity competitionActivity );
}
