package com.example.lorencoo.fdsapp.data.data.team;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity
public class Link {
    @PrimaryKey
    @Embedded
    @SerializedName("players")
    public TeamPlayers teamPlayers= new TeamPlayers();
}
