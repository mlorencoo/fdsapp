package com.example.lorencoo.fdsapp;

import javax.inject.Scope;

@Scope
public @interface ApplicationScope {
}
