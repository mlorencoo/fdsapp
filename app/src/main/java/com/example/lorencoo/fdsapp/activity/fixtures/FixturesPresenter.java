package com.example.lorencoo.fdsapp.activity.fixtures;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.example.lorencoo.fdsapp.api.FootballApi;
import com.example.lorencoo.fdsapp.dao.MyDao;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class FixturesPresenter implements FixturesContract.Presenter, LifecycleObserver {
    private FixturesContract.View view;
    private FootballApi footballApi;
    private MyDao myDao;
    private String competitionId;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public FixturesPresenter(FixturesContract.View view, FootballApi footballApi, MyDao myDao ) {
        this.view = view;
        this.footballApi = footballApi;
        ((LifecycleOwner) this.view).getLifecycle().addObserver(this);
        this.myDao=myDao;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onStart() {
        getFixtures();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onDestroy() {
        compositeDisposable.clear();
    }


    @Override
    public void onTryAgainClick() {
        getFixtures();
    }

    @Override
    public void doYourUpdate() {
        getFixtures();
    }

    @Override
    public void setCompetitionId(String competitionId) {
    this.competitionId=competitionId;
    }

    @Override
    public void searchByTeam(String teamName) {
        getFixturesByTeam(teamName);
    }

    private void getFixturesByTeam(String teamName) {
      view.showDataByTeam(myDao.getFixtureByTeam(teamName));
    }


    private void getFixtures() {
        view.showProgress();
        if (myDao.getFixtureResponseDao(competitionId)==null) {
            compositeDisposable.add(
                    footballApi.getFixturesResponseApi(competitionId)
                    .subscribeOn(Schedulers.io())
                            .flatMapObservable(fixtureResponse -> {
                                    fixtureResponse.setId(competitionId);
                                    myDao.insertFixturesDao(fixtureResponse);
                                    myDao.insertFixturesByTeamDao(fixtureResponse.fixturesList);
                                return Observable.just(fixtureResponse);
                            })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            fixtureResponse -> view.showData(fixtureResponse.fixturesList),
                            throwable -> view.showError()
                    )
            );
        } else {
            view.showData(myDao.getFixtureResponseDao(competitionId).fixturesList);
        }
    }
}
