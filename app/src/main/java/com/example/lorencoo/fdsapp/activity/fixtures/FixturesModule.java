package com.example.lorencoo.fdsapp.activity.fixtures;

import com.example.lorencoo.fdsapp.api.FootballApi;
import com.example.lorencoo.fdsapp.dao.MyDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class FixturesModule {
    private FixturesContract.View view;

    public FixturesModule(FixturesContract.View view) {
        this.view = view;

    }
    @Provides
    @Singleton
    FixturesContract.Presenter provideFixturesPresenter(FootballApi footballApi, MyDao myDao){
        return new FixturesPresenter(view,footballApi,myDao);
    }
}
