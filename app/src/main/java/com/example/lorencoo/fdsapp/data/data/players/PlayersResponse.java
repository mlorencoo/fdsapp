package com.example.lorencoo.fdsapp.data.data.players;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.example.lorencoo.fdsapp.converters.PlayersGsonConventer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@Entity
@TypeConverters(PlayersGsonConventer.class)
public class PlayersResponse {
    @PrimaryKey
    @NonNull
    public int count;

    @SerializedName("players")
    public List<Players> playersList = new ArrayList<>();

    @Expose(deserialize = false, serialize = false)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
