package com.example.lorencoo.fdsapp.activity.competition;

import com.example.lorencoo.fdsapp.api.FootballApi;
import com.example.lorencoo.fdsapp.dao.MyDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CompetitionModule {
    private CompetitionContract.View view;

    public CompetitionModule(CompetitionContract.View view) {
        this.view = view;

    }
    @Provides
    @Singleton
    CompetitionContract.Presenter provideCompetitionPresenter(FootballApi footballApi, MyDao myDao){
        return new CompetitionPresenter(view,footballApi,myDao);
    }
}
