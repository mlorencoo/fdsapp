package com.example.lorencoo.fdsapp.data.data.fixtures;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Result {

    @PrimaryKey
    public String goalsHomeTeam;

    public String goalsAwayTeam;
}
