package com.example.lorencoo.fdsapp.activity.teams;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {TeamsModule.class})
public interface TeamsComponent {
    void inject(TeamsActivity teamsActivity);
}
