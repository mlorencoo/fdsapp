package com.example.lorencoo.fdsapp.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.lorencoo.fdsapp.dao.MyDao;
import com.example.lorencoo.fdsapp.data.data.Competition;
import com.example.lorencoo.fdsapp.data.data.fixtures.FixtureResponse;
import com.example.lorencoo.fdsapp.data.data.fixtures.Fixtures;
import com.example.lorencoo.fdsapp.data.data.players.PlayersResponse;
import com.example.lorencoo.fdsapp.data.data.table.TableResponse;
import com.example.lorencoo.fdsapp.data.data.team.TeamResponse;


@Database(entities = {Competition.class,FixtureResponse.class, Fixtures.class
        , TableResponse.class, TeamResponse.class, PlayersResponse.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract MyDao myDao();
}