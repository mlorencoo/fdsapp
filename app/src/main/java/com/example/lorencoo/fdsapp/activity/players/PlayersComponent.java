package com.example.lorencoo.fdsapp.activity.players;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {PlayersModule.class})
public interface PlayersComponent {
    void inject(PlayersActivity playersActivity);
}
