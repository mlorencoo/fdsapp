package com.example.lorencoo.fdsapp.activity.players;

import com.example.lorencoo.fdsapp.data.data.players.Players;

import java.util.List;

public interface PlayersContract {
    interface View {
        void showError();

        void showProgress();

        void showData(List<Players> playersList);
    }

    interface Presenter {
        void onTryAgainClick();

        void doYourUpdate();

        void setTeamsId(String teamsId);
    }

}
