package com.example.lorencoo.fdsapp.data.data.team;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.example.lorencoo.fdsapp.converters.TeamsGsonConventer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@Entity
@TypeConverters(TeamsGsonConventer.class)
public class TeamResponse {

    @PrimaryKey
    public int count;
    @SerializedName("teams")
    public List<Teams> teamsList =new ArrayList<>();

    @Expose(deserialize = false, serialize = false)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
