package com.example.lorencoo.fdsapp.menus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.activity.FlashScoreActivity;
import com.example.lorencoo.fdsapp.activity.MainActivity;
import com.example.lorencoo.fdsapp.activity.competition.CompetitionActivity;

public abstract class BaseMenuActivity extends AppCompatActivity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        if (R.id.menu_item_main_activity == item.getItemId()) {
            openMenuMainActivity();
        } else if (R.id.menu_item_competition_activity == item.getItemId()) {
            openMenuCompetitionActivity();
        }else if (R.id.menu_item_flashscore_activity == item.getItemId()) {
            openMenuFlashscore();
        }

        return true;
    }

    private void openMenuMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }
    private void openMenuCompetitionActivity() {
        startActivity(new Intent(this, CompetitionActivity.class));
    }
    private void openMenuFlashscore() {
        startActivity(new Intent(this, FlashScoreActivity.class));
    }

}
