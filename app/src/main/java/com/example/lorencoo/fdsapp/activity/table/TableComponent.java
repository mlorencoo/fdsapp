package com.example.lorencoo.fdsapp.activity.table;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {TableModule.class})
public interface TableComponent {
    void inject(TableActivity tableActivity);
}
