package com.example.lorencoo.fdsapp.data.data.fixtures;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.example.lorencoo.fdsapp.converters.FixturesGsonConventer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@Entity
@TypeConverters(FixturesGsonConventer.class)
public class FixtureResponse {
    @PrimaryKey
    @NonNull
    public String count;

    @SerializedName("fixtures")
    public List<Fixtures> fixturesList =new ArrayList<>();

    @Expose(deserialize = false, serialize = false)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
