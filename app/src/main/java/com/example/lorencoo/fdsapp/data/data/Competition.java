package com.example.lorencoo.fdsapp.data.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Competition {

    @PrimaryKey
    @NonNull
    public String id;

    public String caption;


}
