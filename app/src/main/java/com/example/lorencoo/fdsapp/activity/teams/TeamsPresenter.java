package com.example.lorencoo.fdsapp.activity.teams;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.example.lorencoo.fdsapp.api.FootballApi;
import com.example.lorencoo.fdsapp.dao.MyDao;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class TeamsPresenter implements TeamsContract.Presenter, LifecycleObserver {
    private TeamsContract.View view;
    private FootballApi footballApi;
    private MyDao myDao;
    private String competitionId;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public TeamsPresenter(TeamsContract.View view, FootballApi footballApi, MyDao myDao ) {
        this.view = view;
        this.footballApi = footballApi;
        ((LifecycleOwner) this.view).getLifecycle().addObserver(this);
        this.myDao=myDao;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onStart() {
        getTeams();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onDestroy() {
        compositeDisposable.clear();
    }


    @Override
    public void onTryAgainClick() {
        getTeams();
    }

    @Override
    public void doYourUpdate() {
        getTeams();
    }

    @Override
    public void setCompetitionId(String competitionId) {
        this.competitionId=competitionId;
    }


    private void getTeams() {
        view.showProgress();
        if (myDao.getTeamsResponseDao(competitionId)==null) {
            compositeDisposable.add(footballApi.getTeamsResponseApi(competitionId)
                    .subscribeOn(Schedulers.io())
                    .flatMapObservable(teamResponse -> {
                        teamResponse.setId(competitionId);
                        myDao.insertTeamsDao(teamResponse);
                        return Observable.just(teamResponse);
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            teamResponse -> view.showData(teamResponse.teamsList),
                            throwable -> view.showError()
                    )
            );
        } else {
            view.showData(myDao.getTeamsResponseDao(competitionId).teamsList);
        }
    }
}
