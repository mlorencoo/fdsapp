package com.example.lorencoo.fdsapp.converters;

import android.arch.persistence.room.TypeConverter;

import com.example.lorencoo.fdsapp.data.data.fixtures.Fixtures;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class FixturesGsonConventer {
    @TypeConverter
    public static List<Fixtures> stringToFixtures(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Fixtures>>() {}.getType();
        List<Fixtures> fixtures = gson.fromJson(json, type);
        return fixtures;
    }

    @TypeConverter
    public static String fixturesToSting(List<Fixtures> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Fixtures>>() {}.getType();
        String json = gson.toJson(list, type);
        return json;
    }
}
