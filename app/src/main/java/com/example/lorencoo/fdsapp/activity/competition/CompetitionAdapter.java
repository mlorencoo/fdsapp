package com.example.lorencoo.fdsapp.activity.competition;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.activity.fixtures.FixturesActivity;
import com.example.lorencoo.fdsapp.activity.table.TableActivity;
import com.example.lorencoo.fdsapp.activity.teams.TeamsActivity;
import com.example.lorencoo.fdsapp.data.data.Competition;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CompetitionAdapter extends RecyclerView.Adapter<CompetitionAdapter.ViewHolder> {

    public static final String COMPETITION_ID = "COMPETITION_ID";
    private List<Competition> competitionList = new ArrayList<>();
    private int selectedItemPosition = -1;

    public void updateCompetitions(List<Competition> competitions) {
        competitionList.clear();
        competitionList.addAll(competitions);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_competition, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setupFootball(competitionList.get(position));

    }

    @Override
    public int getItemCount() {
        return competitionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_caption_card_view)
        CardView cardView;
        @BindView(R.id.item_competition_text)
        TextView competitionText;

        @BindView(R.id.linear_caption)
        LinearLayout linearLayout;

        private String id;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setupFootball(Competition competition) {
            competitionText.setText(competition.caption);
            id = competition.id;

            if (selectedItemPosition == getAdapterPosition()) {
                linearLayout.setVisibility(View.VISIBLE);
            } else {
                linearLayout.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(view -> {
                int currentPosition = getAdapterPosition();

                if (currentPosition == selectedItemPosition) {
                    selectedItemPosition = -1;
                    notifyItemChanged(currentPosition);
                } else {
                    final int previouslySelectedItem = selectedItemPosition;
                    selectedItemPosition = -1;
                    notifyItemChanged(previouslySelectedItem);
                    selectedItemPosition = currentPosition;
                    notifyItemChanged(currentPosition);
                }
            });
        }

        @OnClick(R.id.item_caption_fixtures)
        public void openFixturesActivity() {
            Intent intentFixtures = new Intent(itemView.getContext(), FixturesActivity.class);
            intentFixtures.putExtra(COMPETITION_ID, id);
            itemView.getContext().startActivity(intentFixtures);
        }

        @OnClick(R.id.item_caption_teams)
        public void openTeamsActivity() {
            Intent intentTeams = new Intent(itemView.getContext(), TeamsActivity.class);
            intentTeams.putExtra(COMPETITION_ID, id);
            itemView.getContext().startActivity(intentTeams);
        }

        @OnClick(R.id.item_caption_table)
        public void openTableActivity() {
            Intent intentTable = new Intent(itemView.getContext(), TableActivity.class);
            intentTable.putExtra(COMPETITION_ID, id);
            itemView.getContext().startActivity(intentTable);
        }
    }
}
