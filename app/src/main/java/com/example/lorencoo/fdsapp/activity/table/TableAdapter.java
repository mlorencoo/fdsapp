package com.example.lorencoo.fdsapp.activity.table;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lorencoo.fdsapp.R;
import com.example.lorencoo.fdsapp.data.data.table.Standing;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TableAdapter extends RecyclerView.Adapter<TableAdapter.ViewHolder> {

    private List<Standing> standingList = new ArrayList<>();

    public void updateTable(List<Standing> standings) {
        standingList.clear();
        standingList.addAll(standings);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_table, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setupTable(standingList.get(position));

    }

    @Override
    public int getItemCount() {
        return standingList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_position)
        TextView position;
        @BindView(R.id.item_table_name)
        TextView name;
        @BindView(R.id.item_table_played_games)
        TextView playedGames;
        @BindView(R.id.item_table_points)
        TextView points;
        @BindView(R.id.item_table_goals)
        TextView goals;
        @BindView(R.id.item_table_goals_against)
        TextView goalsAgainst;
        @BindView(R.id.item_table_goals_difference)
        TextView goalsDifference;
        @BindView(R.id.item_table_wins)
        TextView wins;
        @BindView(R.id.item_table_draws)
        TextView draw;
        @BindView(R.id.item_table_losses)
        TextView loses;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setupTable(Standing standingList) {
            position.setText(String.valueOf(standingList.getPosition()));
            name.setText(standingList.getTeamName());
            playedGames.setText(String.valueOf(standingList.getPlayedGames()));
            points.setText(String.valueOf(standingList.getPoints()));
            goals.setText(String.valueOf(standingList.getGoals()));
            goalsAgainst.setText(String.valueOf(standingList.getGoalsAgainst()));
            goalsDifference.setText(String.valueOf(standingList.getGoalDifference()));
            wins.setText(String.valueOf(standingList.getWins()));
            draw.setText(String.valueOf(standingList.getDraws()));
            loses.setText(String.valueOf(standingList.getLosses()));
        }

    }
}
