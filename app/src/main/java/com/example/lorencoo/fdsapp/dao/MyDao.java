package com.example.lorencoo.fdsapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.lorencoo.fdsapp.data.data.Competition;
import com.example.lorencoo.fdsapp.data.data.fixtures.FixtureResponse;
import com.example.lorencoo.fdsapp.data.data.fixtures.Fixtures;
import com.example.lorencoo.fdsapp.data.data.players.PlayersResponse;
import com.example.lorencoo.fdsapp.data.data.table.TableResponse;
import com.example.lorencoo.fdsapp.data.data.team.TeamResponse;

import java.util.List;

@Dao
public interface MyDao {
    @Query("SELECT *FROM Competition")
    public abstract List<Competition> getAllCompetitionDao();
    @Insert
    void insertCompetitionDao(List<Competition> competitionList );



    @Query("SELECT *FROM FixtureResponse WHERE id= :id")
    public abstract FixtureResponse getFixtureResponseDao(String id);
    @Insert
    void insertFixturesDao(FixtureResponse fixtureResponse);

    @Query("SELECT * FROM Fixtures WHERE homeTeamName LIKE :teamName "
            + "OR awayTeamName LIKE :teamName")
    public abstract List<Fixtures> getFixtureByTeam(String teamName);
    @Insert
    void insertFixturesByTeamDao(List<Fixtures> fixturesList);



    @Query("SELECT *FROM TableResponse WHERE id= :id")
    public abstract TableResponse getTableResponseDao(String id);

    @Insert
    void insertTableDao(TableResponse tableResponse );



    @Query("SELECT *FROM TeamResponse WHERE id= :id")
    public abstract TeamResponse getTeamsResponseDao(String id);
    @Insert
    void insertTeamsDao(TeamResponse teamResponse );

    @Query("SELECT *FROM PlayersResponse WHERE id= :id")
    public abstract PlayersResponse getPlayersResponseDao(String id);
    @Insert
    void insertPlayersDao(PlayersResponse playersResponse );
}
